from app import db


class Tanggal(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    myTanggal = db.Column(db.String(30))
    AzabTanggal = db.Column(db.String(30))

class Bulan(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    myBulan = db.Column(db.String(30))
    AzabBulan = db.Column(db.String(30))

class Tahun(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    myTahun = db.Column(db.String(30))
    AzabTahun = db.Column(db.String(30))