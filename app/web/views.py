from flask import Blueprint, render_template, request
from app import db, share
from app.models import Tanggal, Bulan, Tahun

base_blueprint = Blueprint(
    'web', __name__,
    template_folder='templates'
)

hasil = {
    'tanggal':1,
    'bulan':'januari',
    'tahun':'1991',
    'azab':'berhasil'
}

@base_blueprint.route('/', methods=['GET', 'POST'])
def base():
    ok = False
    if request.method == 'POST':
        tanggal = request.form['tanggal']
        bulan = request.form['bulan']
        tahun = request.form['tahun']
        if tanggal and bulan and tahun:
            mytanggal = Tanggal.query.filter_by(myTanggal=tanggal).first()
            if not mytanggal:
                return 'salah form'

            mybulan = Bulan.query.filter_by(myBulan=bulan).first()
            if not mybulan:
                return 'salah form'

            mytahun = Tahun.query.filter_by(myTahun=tahun).first()
            if not mytahun:
                return 'salah form'

            hasil = mytanggal.AzabTanggal.title() +" "+ mybulan.AzabBulan.title() +" "+ mytahun.AzabTahun.title()
            ok = True
        else:
            hasil = 'lengkapi semua form'
    else:
        hasil = ''       
    return render_template('base.html', hasil=hasil, ok = ok )