import os

from flask import Flask
from flask_share import Share
from flask_sqlalchemy import SQLAlchemy
from app.config import Config

app = Flask(__name__)
app.config.from_object(Config)

db =  SQLAlchemy(app)
share = Share(app)


from app.web.views import base_blueprint 

app.register_blueprint(base_blueprint)