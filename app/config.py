import os

class Config:
    SECRET_KEY = 'rahasia-rahasia'
    SQLALCHEMY_DATABASE_URI = 'mysql://daname:dbpassword@localhost/azab_generator'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DEBUG = True
